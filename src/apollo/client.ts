import { ApolloClient, createHttpLink, GraphQLRequest } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { cache, currentUserVar  } from "apollo/cache";

const httpLink = createHttpLink({
    uri: "http://localhost:4000/graphql",
});

function contextSetter(_: GraphQLRequest, { headers }: any) {
    const token = currentUserVar()?.token;

    return {
        headers: {
            ...headers,
            authorization: token ? token : ""
        }
    }
}

export const client = new ApolloClient({
    link: setContext(contextSetter).concat(httpLink),
    cache: cache,
});